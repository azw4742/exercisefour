package com.example.andrew.exercise4;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;

import java.util.Random;
import java.util.concurrent.TimeUnit;

public class exercise4 extends AppCompatActivity
{
    private View.OnClickListener myOnlyhandler;
    private ImageView card1;
    private ImageView card2;
    private ImageView card3;
    private ImageView card4;
    private ImageView card5;
    private ImageView card6;
    private ImageView card7;
    private ImageView card8;
    private ImageView card9;
    private ImageView card10;
    private ImageView card11;
    private ImageView card12;
    private ImageView card13;
    private ImageView card14;
    private ImageView card15;
    private ImageView card16;
    private ImageView card17;
    private ImageView card18;
    private ImageView card19;
    private ImageView card20;
    private ImageView[] allCards = {card1,card2,card3,card4,card5,card6,card7,card8,card9,card10,card11,card12,card13,card14,card15,card16,card17,card18,card19,card20};
    private int[] position = {0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19};
    private int[] myImageList =
            {
                    R.drawable.nine_of_clubs, R.drawable.nine_of_spades,
                    R.drawable.nine_of_diamonds,R.drawable.nine_of_hearts,
                    R.drawable.ace_of_clubs, R.drawable.ace_of_spades,
                    R.drawable.ace_of_diamonds, R.drawable.ace_of_hearts,
                    R.drawable.jack_of_diamonds,R.drawable.jack_of_hearts,
                    R.drawable.jack_of_clubs, R.drawable.jack_of_spades,
                    R.drawable.king_of_clubs, R.drawable.king_of_spades,
                    R.drawable.king_of_diamonds,R.drawable.king_of_hearts,
                    R.drawable.queen_of_diamonds,R.drawable.queen_of_hearts,
                    R.drawable.queen_of_clubs, R.drawable.queen_of_spades
            };
    private String[] tags = {"nine", "nine", "nine", "nine",
                             "ace", "ace", "ace", "ace",
                             "jack", "jack", "jack", "jack",
                             "king", "king", "king", "king",
                             "queen", "queen","queen", "queen"};
    private int firstTouchCardId;
    private String firstTouchTag;
    private int secondTouchCardId;
    private String secondTouchTag = "";
    private int tapCounter = 0;
    private int missedNum = 0;
    private int correctNum = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_exercise4);
        start();
    }

    public void randomizeArray(int[] array){
        Random rgen = new Random();  // Random number generator

        for (int i=0; i<array.length; i++)
        {
            int randomPosition = rgen.nextInt(array.length);
            int temp = array[i];
            array[i] = array[randomPosition];
            array[randomPosition] = temp;
        }
        position = array;
    }

    public void matchFail()
    {
        if(!(firstTouchTag.equals(secondTouchTag)))
        {
            final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setTitle("Not Matched!");
            alertDialog.setMessage("Not Matched!");
            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    notMatched();
                    alertDialog.dismiss();
                }
            });
            alertDialog.show();
        }
        else if(firstTouchTag.equals(secondTouchTag))
        {
            final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setTitle("Matched!");
            alertDialog.setMessage("Matched!");
            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    Matched();
                    alertDialog.dismiss();
                }
            });
            alertDialog.show();
        }
    }

    public void Matched()
    {
        ImageView tempCard1 = (ImageView) findViewById(firstTouchCardId);
        ImageView tempCard2 = (ImageView) findViewById(secondTouchCardId);
        tempCard1.setVisibility(View.INVISIBLE);
        tempCard1.setClickable(false);
        tempCard2.setVisibility(View.INVISIBLE);
        tempCard2.setClickable(false);
        EditText correct = (EditText) findViewById(R.id.Correct);
        correctNum = correctNum + 1;
        correct.setText("Correct: " + correctNum);
        tapCounter = 0;
        if(correctNum == 10)
        {
            final AlertDialog alertDialog = new AlertDialog.Builder(this).create();
            alertDialog.setCanceledOnTouchOutside(false);
            alertDialog.setTitle("Winner!!");
            alertDialog.setMessage("You have won!\n" + "You Missed: " + missedNum);
            alertDialog.setButton("OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {
                    reset();
                }
            });
            alertDialog.show();
        }
    }


    public void cardClick(View v)
    {
        card1 = (ImageView) findViewById(R.id.card1);
        card2 = (ImageView) findViewById(R.id.card2);
        card3 = (ImageView) findViewById(R.id.card3);
        card4 = (ImageView) findViewById(R.id.card4);
        card5 = (ImageView) findViewById(R.id.card5);
        card6 = (ImageView) findViewById(R.id.card6);
        card7 = (ImageView) findViewById(R.id.card7);
        card8 = (ImageView) findViewById(R.id.card8);
        card9 = (ImageView) findViewById(R.id.card9);
        card10 = (ImageView) findViewById(R.id.card10);
        card11 = (ImageView) findViewById(R.id.card11);
        card12 = (ImageView) findViewById(R.id.card12);
        card13 = (ImageView) findViewById(R.id.card13);
        card14 = (ImageView) findViewById(R.id.card14);
        card15 = (ImageView) findViewById(R.id.card15);
        card16 = (ImageView) findViewById(R.id.card16);
        card17 = (ImageView) findViewById(R.id.card17);
        card18 = (ImageView) findViewById(R.id.card18);
        card19 = (ImageView) findViewById(R.id.card19);
        card20 = (ImageView) findViewById(R.id.card20);
        if(tapCounter == 1)
        {
            switch (v.getId())
            {
                case R.id.card1:
                    card1.setImageResource(myImageList[position[0]]);
                    secondTouchTag = tags[position[0]];
                    secondTouchCardId = v.getId();
                    break;
                case R.id.card2:
                    int cardPosition = position[1];
                    card2.setImageResource(myImageList[cardPosition]);
                    secondTouchTag = tags[cardPosition];
                    secondTouchCardId = v.getId();
                    break;
                case R.id.card3:
                    card3.setImageResource(myImageList[position[2]]);
                    secondTouchTag = tags[position[2]];
                    secondTouchCardId = v.getId();
                    break;
                case R.id.card4:
                    card4.setImageResource(myImageList[position[3]]);
                    secondTouchTag = tags[position[3]];
                    secondTouchCardId = v.getId();
                    break;
                case R.id.card5:
                    card5.setImageResource(myImageList[position[4]]);
                    secondTouchTag = tags[position[4]];
                    secondTouchCardId = v.getId();
                    break;
                case R.id.card6:
                    card6 = (ImageView) findViewById(R.id.card6);
                    card6.setImageResource(myImageList[position[5]]);
                    secondTouchTag = tags[position[5]];
                    secondTouchCardId = v.getId();
                    break;
                case R.id.card7:
                    card7.setImageResource(myImageList[position[6]]);
                    secondTouchTag = tags[position[6]];
                    secondTouchCardId = v.getId();
                    break;
                case R.id.card8:
                    card8.setImageResource(myImageList[position[7]]);
                    secondTouchTag = tags[position[7]];
                    secondTouchCardId = v.getId();
                    break;
                case R.id.card9:
                    card9.setImageResource(myImageList[position[8]]);
                    secondTouchTag = tags[position[8]];
                    secondTouchCardId = v.getId();
                    break;
                case R.id.card10:
                    card10.setImageResource(myImageList[position[9]]);
                    secondTouchTag = tags[position[9]];
                    secondTouchCardId = v.getId();
                    break;
                case R.id.card11:
                    card11.setImageResource(myImageList[position[10]]);
                    secondTouchTag = tags[position[10]];
                    secondTouchCardId = v.getId();
                    break;
                case R.id.card12:
                    card12.setImageResource(myImageList[position[11]]);
                    secondTouchTag = tags[position[11]];
                    secondTouchCardId = v.getId();
                    break;
                case R.id.card13:
                    card13.setImageResource(myImageList[position[12]]);
                    secondTouchTag = tags[position[12]];
                    secondTouchCardId = v.getId();
                    break;
                case R.id.card14:
                    card14.setImageResource(myImageList[position[13]]);
                    secondTouchTag = tags[position[13]];
                    secondTouchCardId = v.getId();
                    break;
                case R.id.card15:
                    card15.setImageResource(myImageList[position[14]]);
                    secondTouchTag = tags[position[14]];
                    secondTouchCardId = v.getId();
                    break;
                case R.id.card16:
                    card16.setImageResource(myImageList[position[15]]);
                    secondTouchTag = tags[position[15]];
                    secondTouchCardId = v.getId();
                    break;
                case R.id.card17:
                    card17.setImageResource(myImageList[position[16]]);
                    secondTouchTag = tags[position[16]];
                    secondTouchCardId = v.getId();
                    break;
                case R.id.card18:
                    card18.setImageResource(myImageList[position[17]]);
                    secondTouchTag = tags[position[17]];
                    secondTouchCardId = v.getId();
                    break;
                case R.id.card19:
                    card19.setImageResource(myImageList[position[18]]);
                    secondTouchTag = tags[position[18]];
                    secondTouchCardId = v.getId();
                    break;
                case R.id.card20:
                    card20.setImageResource(myImageList[position[19]]);
                    secondTouchTag = tags[position[19]];
                    secondTouchCardId = v.getId();
                    break;
            }
            matchFail();
        }
        else if(tapCounter == 0)
        {
            switch (v.getId())
            {
                case R.id.card1:
                    int cardPosition = position[0];
                    card1.setImageResource(myImageList[cardPosition]);
                    firstTouchTag = tags[cardPosition];
                    firstTouchCardId = v.getId();
                    break;
                case R.id.card2:
                    card2.setImageResource(myImageList[position[1]]);
                    firstTouchTag = tags[position[1]];
                    firstTouchCardId = v.getId();
                    break;
                case R.id.card3:
                    card3.setImageResource(myImageList[position[2]]);
                    firstTouchTag = tags[position[2]];
                    firstTouchCardId = v.getId();
                    break;
                case R.id.card4:
                    card4.setImageResource(myImageList[position[3]]);
                    firstTouchTag = tags[position[3]];
                    firstTouchCardId = v.getId();
                    break;
                case R.id.card5:
                    card5.setImageResource(myImageList[position[4]]);
                    firstTouchTag = tags[position[4]];
                    firstTouchCardId = v.getId();
                    break;
                case R.id.card6:
                    card6.setImageResource(myImageList[position[5]]);
                    firstTouchTag = tags[position[5]];
                    firstTouchCardId = v.getId();
                    break;
                case R.id.card7:
                    card7.setImageResource(myImageList[position[6]]);
                    firstTouchTag = tags[position[6]];
                    firstTouchCardId = v.getId();
                    break;
                case R.id.card8:
                    card8.setImageResource(myImageList[position[7]]);
                    firstTouchTag = tags[position[7]];
                    firstTouchCardId = v.getId();
                    break;
                case R.id.card9:
                    card9.setImageResource(myImageList[position[8]]);
                    firstTouchTag = tags[position[8]];
                    firstTouchCardId = v.getId();
                    break;
                case R.id.card10:
                    card10.setImageResource(myImageList[position[9]]);
                    firstTouchTag = tags[position[9]];
                    firstTouchCardId = v.getId();
                    break;
                case R.id.card11:
                    card11.setImageResource(myImageList[position[10]]);
                    firstTouchTag = tags[position[10]];
                    firstTouchCardId = v.getId();
                    break;
                case R.id.card12:
                    card12.setImageResource(myImageList[position[11]]);
                    firstTouchTag = tags[position[11]];
                    firstTouchCardId = v.getId();
                    break;
                case R.id.card13:
                    card13.setImageResource(myImageList[position[12]]);
                    firstTouchTag = tags[position[12]];
                    firstTouchCardId = v.getId();
                    break;
                case R.id.card14:
                    card14.setImageResource(myImageList[position[13]]);
                    firstTouchTag = tags[position[13]];
                    firstTouchCardId = v.getId();
                    break;
                case R.id.card15:
                    card15.setImageResource(myImageList[position[14]]);
                    firstTouchTag = tags[position[14]];
                    firstTouchCardId = v.getId();
                    break;
                case R.id.card16:
                    card16.setImageResource(myImageList[position[15]]);
                    firstTouchTag = tags[position[15]];
                    firstTouchCardId = v.getId();
                    break;
                case R.id.card17:
                    card17.setImageResource(myImageList[position[16]]);
                    firstTouchTag = tags[position[16]];
                    firstTouchCardId = v.getId();
                    break;
                case R.id.card18:
                    card18.setImageResource(myImageList[position[17]]);
                    firstTouchTag = tags[position[17]];
                    firstTouchCardId = v.getId();
                    break;
                case R.id.card19:
                    card19.setImageResource(myImageList[position[18]]);
                    firstTouchTag = tags[position[18]];
                    firstTouchCardId = v.getId();
                    break;
                case R.id.card20:
                    card20.setImageResource(myImageList[position[19]]);
                    firstTouchTag = tags[position[19]];
                    firstTouchCardId = v.getId();
                    break;
            }
            tapCounter = tapCounter + 1;
        }
    }

    public void notMatched()
    {
            ImageView tempCard1 = (ImageView) findViewById(firstTouchCardId);
            ImageView tempCard2 = (ImageView) findViewById(secondTouchCardId);
            tempCard1.setImageResource(R.drawable.back);
            tempCard2.setImageResource(R.drawable.back);
            EditText missed = (EditText) findViewById(R.id.missed);
            missedNum = missedNum +1;
            missed.setText("Missed: " + missedNum);
            tapCounter = 0;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_exercise4, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void start()
    {
        randomizeArray(position);
    }

    public void reset()
    {
        Intent i = getBaseContext().getPackageManager()
                .getLaunchIntentForPackage( getBaseContext().getPackageName() );
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    public void resetClick(View v)
    {
        Intent i = getBaseContext().getPackageManager()
                .getLaunchIntentForPackage( getBaseContext().getPackageName() );
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }
}
